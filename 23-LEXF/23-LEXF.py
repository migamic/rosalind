import sys
import os
import itertools

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    data = data.split('\n')

    symbols = data[0].split()
    n = int(data[1])

    all_words = itertools.product(symbols,repeat=n)

    for w in all_words:
        print(''.join(w))

