import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def max_matches(a,b):
    # A elements need to be paired with B elements (assume A >= B)
    # Consider all elements of B (all will be matched, not all in A need to be matched)
    # The first element of B has A choices, the second has A-1, etc.

    if a < b:
        a,b = b,a
    
    count = 1
    for i in range(b):
        count *= a-i

    return count


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    string = ut.parse_FASTA(data)[0][1]

    # Observation:
    # There are two disjoint components in the graph, the A-U and the C-G
    # The total number is the product of matches of the two sets

    nA = string.count('A')
    nC = string.count('C')
    nG = string.count('G')
    nU = string.count('U')

    result = max_matches(nA,nU) * max_matches(nC,nG)
    print(result)
