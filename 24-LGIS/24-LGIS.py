import sys
import os
import numpy as np
import itertools

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def longest_increasing_subsequence(X):
    # From https://en.wikipedia.org/wiki/Longest_increasing_subsequence
    N = len(X)
    P = np.empty(N, dtype=int)
    M = np.concatenate([[-1],P])
    L = 0

    for i in range(N):

        lo = 1
        hi = L+1
        while lo < hi:
            mid = lo + int((hi-lo)/2)
            if X[M[mid]] >= X[i]: hi = mid
            else: lo = mid+1
        newL = lo

        P[i] = M[newL-1]
        M[newL] = i

        if newL > L: L = newL

    S = np.empty(L, dtype=int)
    k = M[L]
    for j in range(L-1,-1,-1):
        S[j] = X[k]
        k = P[k]

    return S


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])

    numbers = np.array(data.split('\n')[1].split(), dtype=int)

    incr = longest_increasing_subsequence(numbers)
    print(' '.join(incr.astype(str).tolist()))

    decr = -longest_increasing_subsequence(-numbers)
    print(' '.join(decr.astype(str).tolist()))
