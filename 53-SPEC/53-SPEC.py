import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])

    spectrum = [float(i) for i in data.split('\n')]
    diffs = [spectrum[i+1]-spectrum[i] for i in range(len(spectrum)-1)]

    protein = [ut.inv_monisotropic_mass(d) for d in diffs]
    print(''.join(protein))
