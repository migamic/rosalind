import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def compute_new_rabbits(rabbits):
    # Each rabbit pair will make another rabbit pair, except for the 0-age rabbits
    count = 0
    for r in rabbits[1:]:
        count += r
    return count

def grow_rabbits(rabbits):
    for i in range(len(rabbits)-1,0,-1):
        rabbits[i] = rabbits[i-1]
    rabbits[0] = 0

def total_rabbits(rabbits):
    count = 0
    for r in rabbits:
        count += r
    return count

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    n,m = map(int, data.split())

    rabbits = [0] * m    # rabbits[i] indicates the number of pairs of rabbits 'i' months old
    rabbits[0] = 1
    for month in range(n-1):
        #print(month+1, rabbits)
        new_rab = compute_new_rabbits(rabbits)
        grow_rabbits(rabbits)
        rabbits[0] = new_rab

    print(total_rabbits(rabbits))
