import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    N = int(data)

    # Observation, a subset of size N can be represented by the state of N binary variables
    # (i.e. an element is in the set or not). Thus, the total number of subsets is 2^N

    print((2**N) % 1000000)
