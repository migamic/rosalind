import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def p_distance(s1,s2):
    assert len(s1) == len(s2)
    
    count = 0
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            count += 1

    return count/len(s1)



def build_table(strings):
    table = []
    for i in range(len(strings)):
        row = []
        for j in range(len(strings)):
            row.append(p_distance(strings[i], strings[j]))
        table.append(row)

    return table


def print_table(table):
    for row in table:
        print(' '.join(['{0:.5f}'.format(i) for i in row]))



if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    strings = [i[1] for i in ut.parse_FASTA(data)]
    table = build_table(strings)
    print_table(table)
