import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def get_nodes(tree):
    nodes = []
    if tree['name'] is not None:
        nodes.append(tree['name'])

    for c in tree['children']:
        nodes += get_nodes(c)

    return nodes


# Returns sets of nodes that can be obtained by cutting the tree
# (assumed that the rest of nodes are part of the other half of the cut)
# If n>0, only those cuts where there are at least two elements in every side is returned
def find_cuts(tree, n=0):
    cuts = []

    # Try cutting every child
    for c in tree['children']:
        c_nodes = get_nodes(c)
        if len(c_nodes) > 1 and len(c_nodes) < n-1:
            cuts.append(c_nodes)

    # Recursively try cutting subtrees
    for c in tree['children']:
        cuts += find_cuts(c,n)

    return cuts




def character_table(tree):
    nodes = sorted(get_nodes(tree)) # Taxa in lexicographical order
    n = len(nodes)

    cuts = find_cuts(tree, n)

    # Print cuts
    for c in cuts:
        row = [1 if taxa in c else 0 for taxa in nodes]
        print(''.join(map(str,row)))



if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    tree = ut.parse_newick(data)

    character_table(tree)
