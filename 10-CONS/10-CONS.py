import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

def base2idx(b):
    if b == 'A': return 0
    if b == 'C': return 1
    if b == 'G': return 2
    if b == 'T': return 3
    raise Exception(f'Unknown base: {b}')

def idx2base(idx):
    if idx == 0: return 'A'
    if idx == 1: return 'C'
    if idx == 2: return 'G'
    if idx == 3: return 'T'
    raise Exception(f'Unknown idx: {idx}')

def print_profile_matrix(profile):
    for i in range(4):
        print(f'{idx2base(i)}:', end='')
        for j in profile[i]: print(f' {j}', end='')
        print()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    data = ut.parse_FASTA(data)

    # Compute profile matrix
    profile = [[0]*len(data[0][1]) for _ in range(4)]
    for d in data:
        for i,b in enumerate(d[1]):
            profile[base2idx(b)][i] += 1

    # Compute consensus
    consensus = []
    for col in range(len(profile[0])):
        max_val = 0
        max_idx = 0
        for row in range(4):
            if profile[row][col] > max_val:
                max_val = profile[row][col]
                max_idx = row
        consensus.append(idx2base(max_idx))

    # Print consensus
    print(''.join(consensus))

    print_profile_matrix(profile)
