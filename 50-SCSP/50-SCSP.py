import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


CACHE = {}


def shortest_common_supersequence(s1,s2):
    if len(s1) == 0:
        return s2
    if len(s2) == 0:
        return s1

    if s1[0] == s2[0]:
        return s1[0] + shortest_common_supersequence(s1[1:], s2[1:])

    if s1 < s2:
        s1,s2 = s2,s1

    if (s1,s2) in CACHE:
        return CACHE[(s1,s2)]

    sup1 = s1[0] + shortest_common_supersequence(s1[1:], s2)
    sup2 = s2[0] + shortest_common_supersequence(s1, s2[1:])

    if len(sup1) < len(sup2):
        best = sup1
    else:
        best = sup2

    CACHE[(s1,s2)] = best

    return best



if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    s1,s2 = data.split('\n')

    print(shortest_common_supersequence(s1,s2))
