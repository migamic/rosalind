import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


# If possible, returns a protein translation of a string starting at index 0
# Else returns none
def translation_0(s):
    recording = False
    prot = []
    for i in range(0, len(s)-3, 3):
        cod = s[i:i+3]
        amino = ut.cod2amino(cod)

        if cod == 'AUG':
            prot.append(amino)
            recording = True
        elif amino == 'Stop':
            if not recording:
                return None
            return ''.join(prot)
        else:
            if not recording:
                return None
            prot.append(amino)

    return None


def possible_translations(s):
    translations = []

    for i in range(len(s)-3):
        prot = translation_0(s[i:])
        if prot is not None:
            translations.append(prot)

    return translations


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    name,string = ut.parse_FASTA(data)[0]

    translations = possible_translations(string.replace('T', 'U'))
    translations += possible_translations(ut.reverse_complement(string).replace('T', 'U'))

    for t in set(translations):
        print(t)
