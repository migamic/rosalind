import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def is_reverse_palindrome(substring):
    return substring == ut.reverse_complement(substring)


def find_reverse_palindromes(string):
    for i in range(len(string)):
        for j in range(4,13):
            if i+j <= len(string):
                if is_reverse_palindrome(string[i:i+j]):
                    print(i+1, j)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])

    full_string = ut.parse_FASTA(data)[0][1]

    find_reverse_palindromes(full_string)
