import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])

    # Reverse the string
    data = data[::-1]

    # Swap A and T
    data = data.replace('A', 't')
    data = data.replace('T', 'A')
    data = data.replace('t', 'T')

    # Swap C and G
    data = data.replace('C', 'g')
    data = data.replace('G', 'C')
    data = data.replace('g', 'G')

    print(data)
