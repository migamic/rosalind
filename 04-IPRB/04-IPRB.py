import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    k, m, n = map(int, data.split())
    total = k+m+n

    # k -> number of AA
    # m -> number of Aa
    # n -> number of aa

    # Prob of dominant phenotype = sum_{for all possible matches} probability of match * probability of dominant phenotype given that match

    kk = ( (k/total) * ((k-1)/(total-1)) ) * 1
    km = ( (k/total) * (( m )/(total-1)) ) * 1
    kn = ( (k/total) * (( n )/(total-1)) ) * 1
    mk = ( (m/total) * (( k )/(total-1)) ) * 1
    mm = ( (m/total) * ((m-1)/(total-1)) ) * 0.75
    mn = ( (m/total) * (( n )/(total-1)) ) * 0.5
    nk = ( (n/total) * (( k )/(total-1)) ) * 1
    nm = ( (n/total) * (( m )/(total-1)) ) * 0.5
    nn = ( (n/total) * ((n-1)/(total-1)) ) * 0

    print(round(kk+km+kn+mk+mm+mn+nk+nm+nn, 5))
