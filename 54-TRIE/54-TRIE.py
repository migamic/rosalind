import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

glob_name = 0

# A trie is a dict, starting on the root node
# Each node will have a name (number), and a dict of edges to other tries
# The edges is a dict (key: name, value: child trie)
# Leaf nodes have an empty dict of children
def build_trie(strings):
    if len(strings) == 0:
        assert False
        return {}

    tmp_children = {}

    for s in strings:
        if len(s) == 0:
            continue

        edge_name = s[0]
        if edge_name in tmp_children:
            tmp_children[edge_name].append(s[1:])
        else:
            tmp_children[edge_name] = [s[1:]]

    global glob_name
    glob_name += 1
    trie = {
        'name' : glob_name,
        'children' : {c : build_trie(tmp_children[c]) for c in tmp_children}
    }

    return trie


def print_trie(trie):
    for edge_name in trie['children']:
        print(f'{trie["name"]} {trie["children"][edge_name]["name"]} {edge_name}')
        print_trie(trie['children'][edge_name])


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    trie = build_trie(data)
    print_trie(trie)
