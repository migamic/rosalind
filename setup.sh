#!/bin/bash

if [ $# -ne 2 ]; then
 echo "Usage: $0 <number> <problem_name>"
  exit 1
fi

number="$1"
prob_name="$2"

prob_name="$(echo "$prob_name" | tr '[:lower:]' '[:upper:]')"

directory_name="${number}-${prob_name}"

if [ ! -d "$directory_name" ]; then
  mkdir "$directory_name"
  echo "Created directory: $directory_name"
else
  echo "Directory '$directory_name' already exists."
  exit
fi

cp template.py "$directory_name/$directory_name.py"
touch "$directory_name/answer.txt"
touch "$directory_name/sample.txt"
