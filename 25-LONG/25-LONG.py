import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


# Returns the number of characters of the maximum overlap of two strings
# If it is not more than half the length, it returns 0
def string_overlap(s1, s2):
    i = min(len(s1),len(s2))
    min_overlap = max(len(s1)//2, len(s2)//2)+1

    while i > min_overlap:
        if s1[-i:] == s2[:i]:
            return i
        i -= 1

    return 0


# Recursively returns the shortest possible superstring given the previous added string to the chain
# If it is not possible to obtain a superstring, it returns an empty one
def recursive_superstring(current_superstring, last_string, strings):
    if len(strings) == 0:
        return current_superstring

    possible_superstrings = []
    for s in strings:
        overlap = string_overlap(last_string, s)
        if overlap > 0:
            new_strings = strings.copy()
            new_strings.remove(s)
            p_sup = recursive_superstring(current_superstring+s[overlap:], s, new_strings)
            if p_sup != -1:
                possible_superstrings.append(p_sup)

    if len(possible_superstrings) == 0:
        return -1

    return min(possible_superstrings, key=len)


def shortest_superstring(strings):
    possible_superstrings = []
    for i,s in enumerate(strings):
        new_strings = strings.copy()
        new_strings.remove(s)
        p_sup = recursive_superstring(s, s, new_strings)
        if p_sup != -1:
            possible_superstrings.append(p_sup)

    assert len(possible_superstrings) > 0

    return min(possible_superstrings, key=len)

def main(data):
    strings = [s[1] for s in ut.parse_FASTA(data)]
    print(shortest_superstring(strings))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)

    main(ut.read_file(sys.argv[1]))
