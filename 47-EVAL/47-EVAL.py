import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    n = int(data[0])
    s = data[1]
    A = [float(i) for i in data[2].split()]

    num_AT = s.count('A') + s.count('T')
    num_CG = s.count('C') + s.count('G')

    results = []
    for a in A:
        # Probability of a random string of length len(s) and CG content 'a', to be equal to s
        p = (1-a)**num_AT * (a)**num_CG * (0.5)**(num_AT+num_CG)

        # Number of trials; i.e. samples of the random experiment
        num = n - len(s) + 1

        # Expected number of times to see an event of probability p when sampling num times
        exp = num*p

        results.append(exp)

    print(' '.join([str(round(r,3)) for r in results]))
