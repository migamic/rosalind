import sys
import os

import numpy as np
from scipy.stats import binom

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    n = int(data)

    results = []
    for i in range(2*n,0,-1):
        results.append(round(np.log10(binom.cdf(i-1, 2*n, p=0.5)), 4))

    print(' '.join(map(str,results)))
