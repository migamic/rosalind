import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def find_subsequence(string1, string2):
    indices = []

    i = 0
    for s2 in string2:
        while i < len(string1):
            if string1[i] == s2:
                indices.append(i)
                i+=1
                break
            i+=1

    assert len(indices) == len(string2)

    return [i+1 for i in indices]


def main(data):
    parsed = ut.parse_FASTA(data)
    string1 = parsed[0][1]
    string2 = parsed[1][1]

    indices = find_subsequence(string1,string2)
    print(' '.join([str(i) for i in indices]))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    main(data)
