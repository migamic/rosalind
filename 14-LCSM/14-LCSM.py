import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

def all_substrings(string):
    substrings = []
    for i in range(len(string)):
        for j in range(i, len(string)):
            substrings.append(string[i:j+1])
    return set(substrings)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.parse_FASTA(ut.read_file(sys.argv[1]))
    strings = [d[1] for d in data]

    substrings = all_substrings(strings[0])

    longest_sub = ''
    longest_len = 0
    for sub in substrings:
        if len(sub) > longest_len:
            common = True
            for s in strings:
                if not sub in s:
                    common = False
                    break
            if common:
                longest_sub = sub
                longest_len = len(sub)
    print(longest_sub)
