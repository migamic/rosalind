import sys
import os
from time import sleep

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


CACHE = {}


def valid_pair(a,b):
    if a == 'A' and b == 'U': return True
    if a == 'U' and b == 'A': return True
    if a == 'C' and b == 'G': return True
    if a == 'G' and b == 'C': return True

    return False


def noncrossing_perfect_matches(string):

    # Base case
    if len(string) == 0:
        return 1

    # Check cache
    if string in CACHE:
        return CACHE[string]


    count = 0

    # Case 1: node 0 is not matched
    count += noncrossing_perfect_matches(string[1:])

    # Case 2: node 0 is matched. Same as with Catalan numbers
    # Try matching the first node with any other
    for i in range(1, len(string)):
        if valid_pair(string[0],string[i]):
            half1 = noncrossing_perfect_matches(string[1:i])
            half2 = noncrossing_perfect_matches(string[i+1:])

            count += half1 * half2 # Note that they can also be 0

    CACHE[string] = count
    return count


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    print(noncrossing_perfect_matches(ut.parse_FASTA(data)[0][1]) % 1_000_000)
