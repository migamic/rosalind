import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

def parse_input(text):
    samples = text.replace("\n", "").split('>')[1:]
    results = []
    for sample in samples:
        results.append((sample[:13], sample[13:]))
    return results

def compute_GC_content(sequence):
    return (sequence.count('G') + sequence.count('C')) / len(sequence)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    samples = parse_input(data)

    best_cont = 0
    best_name = ''
    for s in samples:
        cont = compute_GC_content(s[1])
        if cont > best_cont:
            best_cont = cont
            best_name = s[0]

    print(best_name)
    print("%.6f" % round(best_cont*100, 6))
