import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


# Returns the path from the root to named node n
# The path is a list of ints, where l[i] is the index of the child at depth i (by definition l[0]=0)
def path_from_root(tree, n):
    if tree['name'] == n:
        return [], True # Node found

    for i in range(len(tree['children'])):
        path, found = path_from_root(tree['children'][i], n)
        if found:
            return [i] + path, True

    return [], False # Node n is not in the tree



# Retusn the distance between two named nodes in a tree
def node_distance(tree, n1, n2):
    path_1, found_1 = path_from_root(tree,n1)
    path_2, found_2 = path_from_root(tree,n2)

    assert found_1, f'Node 1 ({n1}) was not found'
    assert found_2, f'Node 2 ({n2}) was not found'

    # Remove the common part of the paths
    while len(path_1)>0 and len(path_2)>0 and path_1[0] == path_2[0]:
        path_1 = path_1[1:]
        path_2 = path_2[1:]

    # Then the distance is the number of remaining steps
    return len(path_1) + len(path_2)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    results = []
    i = 0
    while i < len(data):
        if len(data[i]) > 0:
            tree = ut.parse_newick(data[i])
            n1,n2 = data[i+1].split()

            results.append(node_distance(tree, n1, n2))

            i += 1

        i += 1

    print(' '.join(map(str,results)))
