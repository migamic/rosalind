import sys
import os
import math

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


# Returns true iff num is a valid permutation of {1:N}
def valid_permutation(num, N):
    nums = [int(s) for s in str(num)]
    if len(set(nums)) < N:
        return False
    for i in nums:
        if i == 0:
            return False
        if i > N:
            return False
    return True

# Returns all possible permutations of {1:N}
def permutations(N):
    max_num = int(''.join([str(i) for i in range(N, 0,-1)]))

    perm = []
    for i in range(max_num+1):
        if valid_permutation(i, N):
            perm.append(i)
    return perm


# Given a permutation (a list of N numbers), returns the 2^N possible signed permutations
def sign_permutation(perm):
    if len(perm) == 1:
        return [[-perm[0]], [perm[0]]]

    sign_perm = []

    rest_perms = sign_permutation(perm[1:])

    for rp in rest_perms:
        sign_perm.append([perm[0]] + rp)
        sign_perm.append([-perm[0]] + rp)

    return sign_perm


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    N = int(data)
    perm = permutations(N)

    print(math.factorial(N) * 2**N)

    for p in perm:
        p = [int(x) for x in str(p)] # Single number to list of digits

        sign_perm = sign_permutation(p)
        for sp in sign_perm:
            print(' '.join([str(x) for x in sp]))
