import sys
import os
from scipy.stats import binom

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    k,N = map(int, data.split())

    # Observation: the probability that the offspring of an individual with alleles Aa is also Aa is 1/2, independently of the other parent
    # Since factors are independent, the probability of a child being AaBb if one of the parents is AaBb is 1/2 * 1/2 = 1/4
    # So, every individual in the genetic tree will be AaBb with probability 1/4

    print(round(1- binom.cdf(N-1, 2**k, 0.25), 3))
