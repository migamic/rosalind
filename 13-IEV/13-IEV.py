import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    population = list(map(int, data.split()))

    # Probability of offspring displaying the dominant phenotype
    weights = [1, 1, 1, 0.75, 0.5, 0]

    dominant = 2*sum([population[i]*weights[i] for i in range(6)])
    print(dominant)
