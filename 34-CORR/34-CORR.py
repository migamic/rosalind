import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def is_repeated(s, strings):
    count = 0

    for s2 in strings:
        if s == s2 or ut.reverse_complement(s) == s2:
            count += 1

    return count > 1


def error_correction(strings):

    correct = []
    for s in strings:
        if not s in correct and is_repeated(s, strings):
            correct.append(s)

    corrections = []
    for s in strings:
        if not s in correct:
            for c in correct:
                if ut.hamming_distance(s,c) == 1:
                    corrections.append((s,c))
                    break
                elif ut.hamming_distance(s,ut.reverse_complement(c)) == 1:
                    corrections.append((s,ut.reverse_complement(c)))
                    break

    return corrections


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.parse_FASTA(ut.read_file(sys.argv[1]))
    corrections = error_correction([i[1] for i in data])

    for c in corrections:
        print(f'{c[0]}->{c[1]}')
