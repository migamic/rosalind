# Rosalind

My solutions to the bioinformatics problems from the [Rosalind platform](https://rosalind.info).