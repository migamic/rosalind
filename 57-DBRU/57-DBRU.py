import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut



def de_bruijn_graph(strings):
    edges = []
    for s in strings:
        r = ut.reverse_complement(s)
        edges.append((s[:-1],s[1:]))
        edges.append((r[:-1],r[1:]))

    return set(edges) # Remove duplicates




if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    edges = de_bruijn_graph(data)

    for e1,e2 in edges:
        print(f'({e1}, {e2})')

