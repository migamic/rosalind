import sys
import os
import math

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    string = ut.parse_FASTA(data)[0][1]

    # Observation:
    # There are two disjoint components in the graph, the A-U and the C-G
    # Each component has two sets of 2n nodes, each node is connected only to the n nodes of the other set
    # Thus, for each component, there are n! perfect matches
    # The total number is the product of matches of the two sets

    n1 = string.count('A')
    n2 = string.count('C')

    result = math.factorial(n1) * math.factorial(n2)
    print(result)
