import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    n = int(data[0])
    A = set(map(int, data[1][1:-1].split(', ')))
    B = set(map(int, data[2][1:-1].split(', ')))
    full = set(range(1,n+1))

    print(A.union(B))
    print(A.intersection(B))
    print(A.difference(B))
    print(B.difference(A))
    print(full.difference(A))
    print(full.difference(B))
