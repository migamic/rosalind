import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

def failure_array(string):
    arr = [0]*len(string)
    new_arr = arr.copy()

    active_indices = list(range(1,len(string)))
    new_indices = []

    for i in range(len(string)):
        if len(active_indices) == 0:
            break

        for a in active_indices:
            if string[a] == string[i]:
                new_arr[a] = arr[a-1] + 1
                if a+1 < len(string):
                    new_indices.append(a+1)

        arr = new_arr.copy()
        
        active_indices = new_indices
        new_indices = []


    return arr


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    fa = failure_array(ut.parse_FASTA(data)[0][1])

    print(' '.join([str(i) for i in fa]))
