import sys
import os
sys.setrecursionlimit(2000)

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


CACHE = {}


# The minimum number of edits to turn s1 into s2 (also s2 into s1)
def edit_distance(s1, s2):

    # Base case
    if len(s1) == 0 or len(s2) == 0:
        return max(len(s1),len(s2)) # Only insertions/deletions are possible

    # If the ends are the same, ignore
    if s1[-1] == s2[-1]:
        return edit_distance(s1[:-1], s2[:-1])

    key = tuple(sorted((s1,s2)))
    if key in CACHE:
        return CACHE[key]

    # Else try all possible edits to match the last symbol, and keep the best

    # Insertion in s1 is equivalent to deletion in s2 (avoids one recursive call)
    insertion = edit_distance(s1, s2[:-1])

    deletion = edit_distance(s1[:-1], s2)

    # substitution in s1 implies that the last symbols will match (avoids one recursive call)
    substitution = edit_distance(s1[:-1], s2[:-1])

    dist = min([insertion,deletion,substitution]) + 1
    CACHE[key] = dist

    return dist




if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.parse_FASTA(ut.read_file(sys.argv[1]))

    print(edit_distance(data[0][1], data[1][1]))
