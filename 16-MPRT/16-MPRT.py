import sys
import os
import requests
import re

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


# Returns the starting positions of the N-glycosylation motif: N{P}[ST]{P}
def process_protein(prot):
    t = 'N[^P][ST]+[^P]'
    results = [str(m.start()+1) for m in re.finditer(f'(?={t})', prot)]
    return results


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    for d in data:
        link_id = list(d.split('_'))[0]
        r = requests.get(f'https://rest.uniprot.org/uniprotkb/{link_id}.fasta')
        if r.status_code == 200:
            name,prot = ut.parse_FASTA(r.text)[0]
            results = process_protein(prot)
            if len(results) > 0:
                print(d)
                print(' '.join(results))
        else:
            print('Request error:', r.status_code)
