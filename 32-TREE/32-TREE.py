import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])

    # Observation: a tree will always have n-1 edges

    n = int(data.split('\n')[0])
    num_edges = len(data.split('\n'))-1

    print(n-num_edges-1)
