import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def most_repeated_element(s):
    counts = {}

    # Count all elements
    for e in s:
        if e in counts:
            counts[e] += 1
        else:
            counts[e] = 1

    # Get the max
    max_count = -1
    max_val = -1
    for e in counts:
        if counts[e] > max_count:
            max_count = counts[e]
            max_val = e

    return max_val, max_count




def minkowski_difference(s1,s2):
    s3 = []
    for i in s1:
        for j in s2:
            s3.append(round(i-j,5))
    return s3


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    s1 = [float(i) for i in data[0].split()]
    s2 = [float(i) for i in data[1].split()]

    s3 = minkowski_difference(s1,s2)
    val, count = most_repeated_element(s3)
    print(count)
    print(val)
