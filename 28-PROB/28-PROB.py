import sys
import os
import math

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def prob_random_string(string, cgc):
    prob = 1
    for base in string:
        if base in {'C','G'}:
            prob *= cgc/2
        else:
            prob *= (1-cgc)/2

    return prob


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    string = data.split('\n')[0]
    cg_cont = [float(i) for i in data.split('\n')[1].split()]

    probs = [prob_random_string(string,cgc) for cgc in cg_cont]

    print(' '.join([str(round(math.log10(p),3)) for p in probs]))
