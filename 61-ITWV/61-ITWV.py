import sys
import os
sys.setrecursionlimit(10000)

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

CACHE = {}


def can_interwove(s, str1, str2, suffix=True):

    if len(s) < len(str1)+len(str2):
        return False # Pruned the suffix too much

    if len(str1) == 0:
        return s[-len(str2):] == str2
    if len(str2) == 0:
        return s[-len(str1):] == str1

    key = (s,str1,str2,suffix)
    if key in CACHE:
        return CACHE[key]

    # Check if it works by interweaving the last letter already
    if s[-1] == str1[-1]:
        if can_interwove(s[:-1], str1[:-1], str2, suffix=False):
            CACHE[key] = True
            return True
    if s[-1] == str2[-1]:
        if can_interwove(s[:-1], str1, str2[:-1], suffix=False):
            CACHE[key] = True
            return True

    # Otherwise, try removing the suffix, if not done already
    if not suffix:
        CACHE[key] = False
        return False # The suffix has already been removed, and it is not possible to interweave

    res = can_interwove(s[:-1], str1, str2, suffix=True)
    CACHE[key] = res
    return res


def print_results(results):
    for row in results:
        print(' '.join(map(str,map(int,row))))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    s = data[0]
    strings = data[1:]

    results = []
    for i in range(len(strings)):
        row = []
        for j in range(len(strings)):
            row.append(can_interwove(s, strings[i], strings[j]))
        results.append(row)

    print_results(results)
