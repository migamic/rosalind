import sys
import os
import math

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])

    n = int(data.split()[0])
    k = int(data.split()[1])

    sets = math.comb(n,k)
    print(sets)
    pperms = math.factorial(k)*sets
    print(pperms)
    print(pperms%1000000)
