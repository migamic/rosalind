def read_file(filename):
    try:
        with open(filename, 'r') as file:
            return file.read().rstrip()
    except FileNotFoundError:
        print("File '01.dat' not found.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")

# Given a single string of text in FASTA format, returns a list of pairs (name,string)
def parse_FASTA(text):
    samples = text.split('>')[1:]
    results = []
    for sample in samples:
        lines = sample.split('\n')
        name = lines[0]
        info = ''.join(lines[1:]).replace('\n', '')
        results.append((name, info))
    return results

# Given a string of codons (assuming length multiple of 3), returns the sequence of amino acids
def transcribe(cod_string):
    assert len(cod_string) % 3 == 0

    codons = [cod_string[i:i+3] for i in range(0, len(cod_string), 3)]
    return [cod2amino(cod) for cod in codons]


# Given a 3-letter codon, returns the 1-letter amino acid (or 'Stop')
def cod2amino(cod):
    lines = read_file('../data/RNA_codon_table.txt').split('\n')

    for l in lines:
        c,a = l.split()
        if cod == c:
            return a

    raise Exception(f'Codon {cod} not found in the table')
    # return None

# Given a 1-letter amino acid (or 'Stop'), returns a list of all possible 3-letter codons
def amino2cod(amino):
    lines = read_file('../data/RNA_codon_table.txt').split('\n')

    cods = []

    for l in lines:
        c,a = l.split()
        if amino == a:
            cods.append(c)

    return cods

# Given a DNA string, returns its reverse complement
def reverse_complement(string):
    # Reverse the string
    string = string[::-1]

    # Swap A and T
    string = string.replace('A', 't')
    string = string.replace('T', 'A')
    string = string.replace('t', 'T')

    # Swap C and G
    string = string.replace('C', 'g')
    string = string.replace('G', 'C')
    string = string.replace('g', 'G')

    return string

# Given an amino acid member, returns its monisotropic mass
def monisotropic_mass(m):
    lines = read_file('../data/monisotropic_mass_table.txt').split('\n')

    for l in lines:
        memb,mass= l.split('   ')
        if memb== m:
            return float(mass)
    
    raise Exception(f'Member {m} not found in the monisotropic mass table')


# Given a monisotropic mass, returns its amino acid member
def inv_monisotropic_mass(m):
    tolerance = 0.001
    lines = read_file('../data/monisotropic_mass_table.txt').split('\n')

    for l in lines:
        memb,mass= l.split('   ')
        if abs(float(mass)-m) < tolerance:
            return memb
    
    raise Exception(f'Mass {m} not found in the monisotropic mass table')


def hamming_distance(a,b):
    assert len(a) == len(b), 'The two strings have different length'
    return sum ( a[i] != b[i] for i in range(len(a)) )


# Parses a string representing a tree in Newick format into a rooted tree object
# Rooted trees are represented as hierarchical dict, starting on the root node; each node has:
#  - children: list of trees, for each children. Empty list if it's a leaf
#  - name: name of the node (can be None if unlabeled)
def parse_newick(string, parent=None):

    if len(string) == 0:
        return {'name': None, 'children': []}

    if string.endswith(';'):
        string = string[:-1]

    if string[0] != '(':   # Leaf node
        assert not (',' in string or '(' in string or ')' in string), f'Wrongly formatted string: {string}'
        return {'name': string, 'children': []}

    # Get name of the root
    last_par = string.rfind(')')
    name = string[last_par+1:]
    ch_str = string[1:last_par]

    # Parse children
    children = []
    curr_child = ''
    par_depth = 0
    for c in ch_str:
        if c == ' ':
            continue

        if c == ',' and par_depth == 0:
            children.append(curr_child)
            curr_child = ''
        else:
            curr_child += c

        if c == '(':
            par_depth += 1
        elif c == ')':
            assert par_depth > 0, 'Par depth is negative'
            par_depth -= 1

    children.append(curr_child)

    node = {
        'name' : name if name != '' else None,
        'children' : [parse_newick(child) for child in children]
    }

    return node


# Prints a tree in a human-readable way (the format of the tree is defined in the function above)
def print_tree(tree, depth=0):
    prefix = '--' * depth
    name = tree['name'] if tree['name'] is not None else '.'
    print(f'{prefix} {name}')

    for child in tree['children']:
        print_tree(child, depth+1)
