import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    data = ut.parse_FASTA(data)

    string = data[0][1]

    for intron in list(zip(*data[1:]))[1]:
        string = string.replace(intron, '')

    rna = string.replace('T', 'U')
    aminos = ut.transcribe(rna)

    print(''.join(aminos[:-1])) # Remove the last stop amino acid
