import sys
import os
sys.setrecursionlimit(2000)

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


CACHE = {}


# The minimum number of edits to turn s1 into s2 (also s2 into s1)
def edit_distance_alignment(s1, s2):

    # Base case
    if len(s1) == 0 and len(s2) == 0:
        return (0,'','')
    elif len(s1) == 0:
        return (len(s2), '-'*len(s2), s2)
    elif len(s2) == 0:
        return (len(s1), s1, '-'*len(s1))

    # If the ends are the same, ignore
    if s1[-1] == s2[-1]:
        (dist, a1, a2) = edit_distance_alignment(s1[:-1], s2[:-1])
        return (dist, a1+s1[-1], a2+s2[-1])

    key = (s1,s2)
    if key in CACHE:
        return CACHE[key]

    # Else try all possible edits to match the last symbol, and keep the best

    # Insertion in s1 is equivalent to deletion in s2 (avoids one recursive call)
    (i_d, i_a1, i_a2) = edit_distance_alignment(s1, s2[:-1])

    (d_d, d_a1, d_a2)= edit_distance_alignment(s1[:-1], s2)

    # substitution in s1 implies that the last symbols will match (avoids one recursive call)
    (s_d, s_a1, s_a2)= edit_distance_alignment(s1[:-1], s2[:-1])

    dists = [i_d, d_d, s_d]
    idx_min = dists.index(min(dists))

    min_dist = min(dists) + 1 # Edit distance
    if idx_min == 0:
        a1 = i_a1 + '-'
        a2 = i_a2 + s2[-1]
    elif idx_min == 1:
        a1 = d_a1 + s1[-1]
        a2 = d_a2 + '-'
    elif idx_min == 2:
        a1 = s_a1 + s1[-1]
        a2 = s_a2 + s2[-1]

    res = (min_dist, a1, a2)
    CACHE[key] = res
    return res




if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.parse_FASTA(ut.read_file(sys.argv[1]))

    dist, a1, a2 = edit_distance_alignment(data[0][1], data[1][1])

    print(dist)
    print(a1)
    print(a2)
