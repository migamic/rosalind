import sys
import os
sys.setrecursionlimit(2000)

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


CACHE = {}


def LCS(s1, s2):
    if len(s1) == 0 or len(s2) == 0:
        return []

    if s1 > s2:
        s1,s2 = s2,s1 # To avoid storing twice in cache

    if (s1,s2) in CACHE:
        return CACHE[(s1,s2)]

    if s1[-1] == s2[-1]:
        return LCS(s1[:-1], s2[:-1]) + [s1[-1]]

    lcs1 = LCS(s1[:-1], s2)
    lcs2 = LCS(s1, s2[:-1])

    if len(lcs1) > len(lcs2):
        best = lcs1
    else:
        best = lcs2

    CACHE[(s1,s2)] = best
    return best


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    strings = ut.parse_FASTA(ut.read_file(sys.argv[1]))
    lcs = LCS(strings[0][1], strings[1][1])
    print(''.join(lcs))

