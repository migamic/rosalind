import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def order_strings(symbols, n, partial_string=''):
    if len(partial_string) == n:
        print(partial_string)
        return

    if len(partial_string) > 0:
        print(partial_string)

    for s in symbols:
        order_strings(symbols, n, partial_string+s)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    data = data.split('\n')

    symbols = data[0].split()
    n = int(data[1])

    order_strings(symbols, n)
