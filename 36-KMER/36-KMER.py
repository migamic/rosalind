import sys
import os
import itertools

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def init_decomp(k):
    all_kmers = [i for i in itertools.product(*(['ACGT'] * 4))]
    return {''.join(k):0 for k in all_kmers}



def kmer_decomposition(string, k):
    decomp = init_decomp(k)

    for i in range(len(string)-k+1):
        kmer = string[i:i+k]
        decomp[kmer] += 1

    return [decomp[k] for k in sorted(decomp.keys())]



if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])

    decomp = kmer_decomposition(ut.parse_FASTA(data)[0][1], 4)
    print(' '.join([str(i) for i in decomp]))
