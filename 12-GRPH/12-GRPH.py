import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def overlap(str1, str2):
    assert len(str1) >=3 and len(str2) >= 3
    return str1[-3:] == str2[:3]

def print_edges(edges):
    for v1 in edges.keys():
        for v2 in edges[v1]:
            print(v1, v2)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    data = ut.parse_FASTA(data)

    edges = {}
    for n1 in data:
        edges[n1[0]] = []
        for n2 in data:
            if n1[0] != n2[0]:
                if overlap(n1[1], n2[1]):
                    edges[n1[0]].append(n2[0])

    print_edges(edges)
