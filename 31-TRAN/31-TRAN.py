import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def tran_ratio(string1,string2):
    transitions = 0
    transversions = 0

    for i in range(len(string1)):
        a = string1[i]
        b = string2[i]

        if a != b:
            if (a in {'A','G'} and b in {'A','G'}) or (a in {'C','T'} and b in {'C','T'}):
                transitions += 1
            else:
                transversions += 1

    return transitions/transversions



def main(data):
    parsed = ut.parse_FASTA(data)
    string1 = parsed[0][1]
    string2 = parsed[1][1]

    assert len(string1) == len(string2)

    print(tran_ratio(string1,string2))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    main(data)
