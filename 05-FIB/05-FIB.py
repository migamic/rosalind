import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def compute_rabbits(n, k, results):
    # Base case
    if n<2: return 1

    # Reuse computation if possible
    if results[n] != 0: return results[n]

    # Compute recursively
    result = compute_rabbits(n-1, k, results) + k*compute_rabbits(n-2, k, results)
    results[n] = result
    return result


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    n, k = map(int, data.split())

    results = [0]*n

    print(compute_rabbits(n-1,k,results))
