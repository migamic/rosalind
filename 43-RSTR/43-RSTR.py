import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)

    data = ut.read_file(sys.argv[1]).split('\n')
    N = int(data[0].split(' ')[0])
    x = float(data[0].split(' ')[1])
    s = data[1]

    num_CG = s.count('C') + s.count('G')
    num_AT = s.count('A') + s.count('T')

    # Probability to get a random string equal to s
    # A and T have probability (1-x)*0.5, C and G have probability x*0.5
    PrA = (1-x)**num_AT * (x)**num_CG * (0.5)**(num_CG+num_AT)

    # Probability that none of the N strings equal s
    Pr_None = (1-PrA)**N

    # Probability that at least one equals s
    result = 1-Pr_None

    print(round(result,3))
