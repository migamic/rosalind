# This version is too slow for the large cases. v2 is the one used to get the solution


import sys
import os
from time import sleep

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def valid_pair(a,b):
    if a == 'A' and b == 'U': return True
    if a == 'U' and b == 'A': return True
    if a == 'C' and b == 'G': return True
    if a == 'G' and b == 'C': return True

    return False


# Returns True iff adding the edge i-j would create a crossing with the existing edges
# Assumes:
#   new_match = (i,j) : i < j
#   for all matches (a,b): a<b and a<i
def crossing(matches, new_match):
    (i,j) = new_match

    for (a,b) in matches:
        if a < i < b < j:
            return True
    
    return False


# Returns True iff there is the same amount of (A and U bases) and (C and G bases)
def check_proportion(string):
    return string.count('U') == string.count('A') and string.count('C') == string.count('G')


def can_add_edge(string, i, j, matches):
    if not valid_pair(string[i], string[j]):
        return False

    #if len(side1) % 2 != 0: return False
    #if len(side2) % 2 != 0: return False

    # Check that on every side of the edge there are an even number of nodes, and the correct proportion of each
    if (i+j) % 2 == 0: return False # Same parity will create odd numbers of nodes on every side

    side1 = string[:i] + string[j+1:]
    side2 = string[i+1:j]
    if not check_proportion(side1): return False
    if not check_proportion(side2): return False

    if crossing(matches, (i,j)):
        return False

    return True



def num_perf_matches(string, matches):
    if len(matches)*2 == len(string):
        # Perfect match found
        return 1

    match_count = 0

    used_nodes = [i for j in matches for i in j]
    if len(matches) == 0:
        start_idx = 0
    else:
        start_idx = max([i[0] for i in matches])+1
    #print('Matches:', matches, ' Used nodes:', used_nodes)

    for i in range(start_idx, len(string)):
        if len(matches) == 0: print(i)
        if i not in used_nodes:
            for j in range(i+1, len(string)):
                if len(matches) == 0: print('   ', j)
                if j not in used_nodes:
                    if can_add_edge(string, i, j , matches):
                        match_count += num_perf_matches(string, matches+[(i,j)])

    return match_count




def noncrossing_perfect_matches(string):
    assert check_proportion(string)

    return num_perf_matches(string, [])


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    print(noncrossing_perfect_matches(ut.parse_FASTA(data)[0][1]))
