# Does not work, but I'm not sure why. Some problem with bad caching


import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


CACHE = {}


# Substracts the minimum value from the sequence to make it start at 0
# Makes caching more efficient
def minimize_seq(seq):
    m = min(seq)
    return tuple(i-m for i in seq)


def reversal_ordering(seq, N=10, depth=0):

    # Base case
    if len(seq) == 0:
        return 0

    if depth > N:
        return N # No need to look further, N is an upper bound

    # Prune the ends if already in order
    if seq[0] == min(seq):
        return reversal_ordering(seq[1:])
    if seq[-1] == max(seq):
        return reversal_ordering(seq[:-1])

    seq = minimize_seq(seq)

    if seq in CACHE:
        return CACHE[seq]
    else:
        CACHE[seq] == -1 # Temporarily, to avoid loops

    # Try all possible permutations recursively, then return the minimum
    dist = len(seq)+5 # Upper bound
    cands = []
    for perm_size in range(2,len(seq)+1):
        i = 0
        while i + perm_size <= len(seq):
            perm = seq[:i]+seq[i:i+perm_size][::-1]+seq[i+perm_size:]
            cand = reversal_ordering(perm, N=N, depth=depth+1)

            cands.append((cand, perm))


            if cand == 0:
                CACHE[seq] = 1
                return 1 # No need to check further
            elif cand != -1 and cand+1 < dist:
                dist = cand+1
            i += 1

    CACHE[seq] = dist
    return dist


def reversal_distance(seq1, seq2):
    assert len(seq1) == len(seq2)
    N = len(seq1)

    # For simplicity, assume that the first sequence is ordered 0->9, then the second one needs to be ordered
    for i in range(N):
        val = seq2[i]
        idx = seq1.index(val)
        seq2[i] = idx

    # Compute the reversal distance to go from seq2 to the ordered sequence
    # (note that the items now go from 0 to 9)
    return reversal_ordering(tuple(seq2)) # Make it a tuple so it's hashable


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    results = []
    i = 0
    while i < len(data):
        if len(data[i]) > 0:
            seq1 = [int(num) for num in data[i].split()]
            seq2 = [int(num) for num in data[i+1].split()]
            res = reversal_distance(seq1,seq2)
            results.append(res)
            i += 1

        i += 1

    print(' '.join([str(r) for r in results]))
