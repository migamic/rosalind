# Works, but takes a while. Cache for N=10 takes >1h to compute (and ~80MB of space)
# It is precomputed in disk, then quickly loaded to get the solution quickly


import sys
import os
import pickle

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def build_cache(N):
    base_case = tuple(range(N))
    cache = {base_case:0}
    queue = [base_case]

    while len(queue) > 0:
        top = queue.pop(0)

        # Set of all possible permutations
        perms = []
        for perm_size in range(2,len(top)+1):
            i = 0
            while i + perm_size <= len(top):
                perm = top[:i]+top[i:i+perm_size][::-1]+top[i+perm_size:]
                perms.append(perm)
                i += 1

        for p in perms:
            if p not in cache:
                cache[p] = cache[top] + 1
                queue.append(p)

    return cache


def relative_order(seq1, seq2):
    assert len(seq1) == len(seq2)
    N = len(seq1)

    # For simplicity, assume that the first sequence is ordered 0->9, then the second one needs to be ordered
    for i in range(N):
        val = seq2[i]
        idx = seq1.index(val)
        seq2[i] = idx

    # Note that the items now go from 0 to 9
    return seq2


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    N = 10
    filename = f'cache_{N}.pkl'

    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            cache = pickle.load(f)
    else:
        cache = build_cache(N)
        with open(filename, 'wb') as f:
            pickle.dump(cache, f)


    results = []
    i = 0
    while i < len(data):
        if len(data[i]) > 0:
            seq1 = [int(num) for num in data[i].split()]
            seq2 = [int(num) for num in data[i+1].split()]
            res = cache[tuple(relative_order(seq1,seq2))]
            results.append(res)
            i += 1

        i += 1

    print(' '.join([str(r) for r in results]))
