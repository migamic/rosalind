# Does not work because for large sequences it gets stuck on a dependency loop


import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


CACHE = {}


# Simplify the sequence to make caching more efficient
def minimize_seq(seq):
    # Prune ends
    while len(seq) > 0 and seq[0] == min(seq):
        seq = seq[1:]
    while len(seq) > 0 and seq[-1] == max(seq):
        seq = seq[:-1]

    if len(seq) == 0:
        return seq

    # Substract baseline
    m = min(seq)
    return tuple(i-m for i in seq)


def reversal_ordering(seq):
    seq = minimize_seq(seq)
    queue = [seq]

    while len(queue) > 0:
        p = queue.pop(0)

        # Set of all possible permutations
        perms = []
        for perm_size in range(2,len(p)+1):
            i = 0
            while i + perm_size <= len(p):
                perm = p[:i]+p[i:i+perm_size][::-1]+p[i+perm_size:]
                perms.append(minimize_seq(perm))
                i += 1

        # If all permutations are computed, return the minimum
        # Else put them in the queue and wait until they are
        dists = []
        for perm in perms:
            if len(perm) == 0:
                dists.append(1)
            elif perm in CACHE:
                dists.append(CACHE[perm] + 1)
            else:
                if perm not in queue:
                    queue.append(perm)
                dists.append(-1)

        if 1 in dists:
            CACHE[p] = 1
        elif -1 in dists:
            # Not all permutations had been completed. Put it in the queue again
            queue.append(p)
        else:
            CACHE[p] = min(dists)

    return CACHE[seq]




def reversal_distance(seq1, seq2):
    assert len(seq1) == len(seq2)
    N = len(seq1)

    # For simplicity, assume that the first sequence is ordered 0->9, then the second one needs to be ordered
    for i in range(N):
        val = seq2[i]
        idx = seq1.index(val)
        seq2[i] = idx

    # Compute the reversal distance to go from seq2 to the ordered sequence
    # (note that the items now go from 0 to 9)
    return reversal_ordering(tuple(seq2)) # Make it a tuple so it's hashable


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')


    results = []
    i = 0
    while i < len(data):
        if len(data[i]) > 0:
            seq1 = [int(num) for num in data[i].split()]
            seq2 = [int(num) for num in data[i+1].split()]
            res = reversal_distance(seq1,seq2)
            results.append(res)
            i += 1

        i += 1

    print(' '.join([str(r) for r in results]))
