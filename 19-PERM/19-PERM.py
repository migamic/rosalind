import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


# Returns true iff num is a valid permutation of {1:N}
def valid_permutation(num, N):
    nums = [int(s) for s in str(num)]
    if len(set(nums)) < N:
        return False
    for i in nums:
        if i == 0:
            return False
        if i > N:
            return False
    return True

# Returns all possible permutations of {1:N}
def permutations(N):
    max_num = int(''.join([str(i) for i in range(N, 0,-1)]))

    perm = []
    for i in range(max_num+1):
        if valid_permutation(i, N):
            perm.append(i)
    return perm

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1])
    perm = permutations(int(data))


    print(len(perm))
    for p in perm:
        print(' '.join(str(p)))
