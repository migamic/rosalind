import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
import utils as ut


def find_string_t(masses, acc_weight, used_masses, t=[]):

    if len(t)*2 == len(masses)-2:
        return True, t

    for i in range(len(masses)):
        if not used_masses[i]:
            m = masses[i] - acc_weight
            try:
                found_t = ut.inv_monisotropic_mass(m)
            except:
                continue

            # Try setting this value of t
            used_masses[i] = True
            success, res_t = find_string_t(masses, acc_weight+m, used_masses, t+[found_t])
            if success:
                return success, res_t
            # Else, keep trying
            used_masses[i] = False
            

    return False, []


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <data file>")
        sys.exit(1)
    data = ut.read_file(sys.argv[1]).split('\n')

    parent_mass = float(data[0])
    masses = [float(i) for i in data[1:]]

    n = len(masses)-2   # n = len(t)

    used_masses = [False]*(n+2)
    w1 = min(masses) # It's either w1 or w2. Since the order is not important, let's say w1
    used_masses[masses.index(w1)] = True

    success, t = find_string_t(masses, w1, used_masses)
    assert success, 'No solution found'

    print(''.join(t))
